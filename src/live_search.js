//array de objetos de tipo Personajes


//casilla de texto, boton y caja de resultado
const formulario = document.querySelector('#formulario');
const boton = document.querySelector('#boton');
const resultado = document.querySelector('.link-ch')

//desactivando el boton
document.querySelector('#boton').disabled = true;

//funcion filtrar
const filtrar = async() =>{

    //llamando api usando FetchApi
    const url = await fetch('src/Personajes.json')
    const data = await url.json();
    resultado.innerHTML='';
    //texto del usuario
    const texto = formulario.value.toLowerCase();
    for(let personaje of data.Personajes){
        //guardamos el nombre pasado a traves del input
        let nombre = personaje.nombre
        if(texto === ''){
            resultado.style.height=0
        }
        else{
            resultado.style.height='auto'
        }
        if(nombre.indexOf(texto) !== -1){
            resultado.innerHTML+=`
            <li class="link-item-ch">
                <a href="/character.html?character=${personaje.nombre}">
                    <span>${personaje.nombre}</span>
                    <img src="${personaje.imagen}" alt="" >
                </a>              
            </li>
            `
        }
    }
    if(resultado.innerHTML ===''){
        resultado.innerHTML+=`
        <li class="link-item-ch">
            <a href="">
                <span>Personaje no encontrado :(</span>

            </a>              
        </li>
        `
    }

}

boton.addEventListener('click',filtrar);
formulario.addEventListener('keyup',filtrar);
filtrar();

