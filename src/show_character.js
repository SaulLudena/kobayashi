

const resultadoPersonaje = document.querySelector('.big-card')
const resultadoPersonajeSimilar = document.querySelector('.similar-card-container')
const param = new URLSearchParams(window.location.search);
const paramval = param.get("character").replace(/%20/g, " ");

const showCharacter = async() => {
    try {
        resultadoPersonaje.innerHTML='';

        //buscando buscando el parametro y guardandolo
        //llamando api(json local xd) usando FetchApi
        const url = await fetch('src/Personajes.json');
        const data = await url.json();

        for(let personaje of data.Personajes){
            let nombre = personaje.nombre
            //engine to find a specific character in a array
            if(nombre.indexOf(paramval) !== -1){
                //if a character has a friend so show it up, else show the empty item
                //saul: worst way to hide/show an item in the DOM, but it works
                if(!personaje.mejorAmigo.amigo1){
                    let classFriend= "disableFriend"
                    resultadoPersonaje.innerHTML= `        
                    <div class="big-card-img">
                        <img src="${personaje.imagen}" alt="">
                    </div>
                    <div class="big-card-items">
                        <div class="item-group">
                            <span>Nombre:</span><br>
                            <h1>${personaje.nombre}</h1>
                        </div>
                        <div class="item-group ${classFriend}" >
                            <span>Mejor amigo:</span><br>
                            <a href="/character.html?character=${personaje.mejorAmigo.amigo1}"></a>
                        </div>
                        <div class="item-group">
                            <span>Aparicion:</span><br>
                            <p>Temporada ${personaje.aparicion}</p>
                        </div>
                    </div>
                `
                }else{
                    resultadoPersonaje.innerHTML= `        
                    <div class="big-card-img">
                        <img src="${personaje.imagen}" alt="">
                    </div>
                    <div class="big-card-items">
                        <div class="item-group">
                            <span>Nombre:</span><br>
                            <h1>${personaje.nombre}</h1>
                        </div>
                        <div class="item-group" >
                            <span>Mejor amigo:</span><br>
                            <a href="/character.html?character=${personaje.mejorAmigo.amigo1}">${personaje.mejorAmigo.amigo1}</a>
                        </div>
                        <div class="item-group">
                            <span>Aparicion:</span><br>
                            <p>Temporada ${personaje.aparicion}</p>
                        </div>
                    </div>
                `
                }
                

            }
        }

    } catch (error) {
        console.log(error)
    }
}

const showAllCharacters = async()=>{
    try {
        resultadoPersonajeSimilar.innerHTML='';

        const url = await fetch('src/Personajes.json');
        const data = await url.json();


        data.Personajes.forEach((similarCharacter) => {
            resultadoPersonajeSimilar.innerHTML+=`
            <a href="/character.html?character=${similarCharacter.nombre}" class="similar-card">
                <div class="similar-card-img">
                    <img src=${similarCharacter.imagen} alt="">
                </div>
                <div class="big-card-items">
                    <div class="item-group">
                        <span>${similarCharacter.nombre}</span>
                    </div>
                </div>
            </a>
            `
        })
    } catch (error) {
        console.log(error)
    }
}
showCharacter();
showAllCharacters();